const Api = function (opts) {
	
	const {ctrlProds,ctrlPrecio,ctrlUnidades,ctrlRelProds} = opts

	const precio = {
		aPesos: centavos => centavos / 100,
		render: function (p) {
			ctrlPrecio.html(`$${p}`)
		}
	}

	const unidades = {
		ini: function (val) {
			ctrlUnidades.attr('min', val).val(val)
		}
	}

	const productos = {
		modelo: {
      id: "0", description: "", minQuantity: 0, unit: "", unitPriceInCents: 0
  	},
  	combo: {
  		template: p => `<option value="${p.id}" data-min="${p.minQuantity}" data-price="${p.unitPriceInCents}">${p.description}</option>`,
  		render: (template,ctrl)=>{
  			return arrProds=>{
		  		var html = ''
					for (let i in arrProds)
						html += template(arrProds[i])
					if (html)
						ctrl.html(html)
					return arrProds
  			}
  		},
  		ajax: render=>{
  			return $.get({
  				url: 'https://private-70cb45-aobara.apiary-mock.com/product/list'
  			})
  			.done(render)
  			.fail((xhr, status, err)=>{
	  			console.log(err)
	  		})
  		}
  	},
  	
  	ini: ref=>{
  		// @prom: {func} la siguiente acción luego de render()
  		return prom=>{
  			var {render,template,ajax} = ref.combo
  			ajax(render(template,ctrlProds)).done(prom)
  		}
  	}
	}

	return {
		iniProds: productos.ini(productos),
		/*
		* Recibe un modelo de producto renderiza 
		* el precio y las unidades mínimas
		*/
		renderPrecioUnidades: (p=productos.modelo)=> {
			const {unitPriceInCents, minQuantity} = p
			precio.render(
				precio.aPesos(unitPriceInCents)*minQuantity
			)
			unidades.ini(minQuantity)
		},
		iniRelatedProds: function () {
			var modelo = {"pictureUrl":'',"title":'',"fromPrice":0,"description":''}
			var template = (prod)=>`
				<div class="col-xs-12 col-md-4">
					<span class="relacionados">
	      		<img src="${prod.pictureUrl}" class="img-responsive" alt="imagen del producto" />
	      		<h3>${prod.title}</h3>
	      		<span class="precio"><i class="fa fa-credit-card"></i> desde $${prod.fromPrice}</span>
	      		<p>${prod.description}</p>
	      		<button class="btn ibtn-default">contratar</button>
	    		</span>
    		</div>`

			$.get({
				url: 'https://private-70cb45-aobara.apiary-mock.com/related-product/list'
			})
			.done((data)=>{
				$('.cargando').detach()
				var html = ''
				for (let i in data)
					html += template(data[i])
				ctrlRelProds.html(html)
			})
			.fail((xhr, status, err)=>{
				console.log('error')
			})
		}
	}

}

$(function () {
	const selectores = {
		ctrlProds: $('select#productos'),
		ctrlPrecio: $('span.precio'),
		ctrlUnidades: $('input#unidades'),
		ctrlRelProds: $('div.relacionados div.row')
	}
	
	const api = Api(selectores)
	
	// Al iniciar los productos, tomo el primero y muestro el precio y las unidades
	api.iniProds((data)=>api.renderPrecioUnidades((data.length&&data[0])||{}))

	// Cada vez que cambia el producto seleccionado, actualizo el precio y las unidades
	selectores.ctrlProds.on('change', function (e) {
		var {min,price} = selectores.ctrlProds.find(":selected")[0].dataset
		api.renderPrecioUnidades({
			unitPriceInCents: Number(price),
			minQuantity: Number(min)
		})
	})

	api.iniRelatedProds()
})
