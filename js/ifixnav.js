$(function(){
	
	$('nav.ifixnav a#ingreso').click(function () {
		$(this).next('.form-wrapper').toggle()
	})

	$('nav.ifixnav a.imob-trigger').click(function () {
		$('ul.imob-menu').toggle('fast')
	})

	// formulario login >>>
	$('input[id="usuario"]').parents('.group').addClass('focused')

	$('.input').focus(function(){
	  $(this).parents('.group').addClass('focused')
	})

	$('.input').blur(function(){
	  var inputValue = $(this).val()
	  if ( inputValue == "" ) {
	    $(this).removeClass('filled')
	    $(this).parents('.group').removeClass('focused')  
	  } else {
	    $(this).addClass('filled')
	  }
	})
})