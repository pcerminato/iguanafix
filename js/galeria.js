$(function () {
  // cambiar el estado de la vista
  const cambiarEstado = function (selectorStr,clase,i) {
    $(selectorStr)
      .removeClass(clase)
      .eq(i)
      .addClass(clase)
  }

	$('div.galeria.thumbnails').on('click', 'a', function() {
    /* 
    * Orden del thumb seleccionado. 
    * El orden de los thumbs corresponde con el de su imagen
    */
    const i = $(this).index()
    // thumbs
    cambiarEstado('div.galeria.thumbnails img', 'selected', i)
    // imagen
    cambiarEstado('div.galeria.imagenes img', 'activa', i)
  })
})
