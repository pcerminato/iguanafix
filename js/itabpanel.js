$(function () {
	$('.itabpanel .header').on('click', 'a', function () {
		// index del tab que disparó click
		const i = $(this).index()
		
		$('.itabpanel .header a').removeClass('activo')
		$(this).addClass('activo')

		$('.itabpanel .panel').removeClass('activo')
		$('.itabpanel .panel').eq(i).addClass('activo')
	})
})